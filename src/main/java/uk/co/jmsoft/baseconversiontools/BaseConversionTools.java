/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.baseconversiontools;

import javax.swing.JFrame;

/**
 * Provides static, thread safe, methods for converting strings between binary, octal, decimal
 * and hexadecimal.
 * <p>
 * @author Scooby
 */
public abstract class BaseConversionTools extends JFrame
{

    public static synchronized String binaryToOctal(String binary)
    {
        return Integer.toOctalString(Integer.parseInt(binary, 2));
    }

    public static synchronized String binaryToDecimal(String binaryString)
    {
        return String.valueOf(Integer.parseInt(binaryString, 2));
    }

    public static synchronized String binaryToHexadecimal(String binaryString)
    {
        return Integer.toHexString(Integer.parseInt(binaryString, 2));
    }

    public static synchronized String octalToBinary(String octalString)
    {
        return Integer.toBinaryString(Integer.parseInt(octalString, 8));
    }

    public static synchronized String octalToDecimal(String octalString)
    {
        return String.valueOf(Integer.parseInt(octalString, 8));
    }

    public static synchronized String octalToHexadecimal(String octalString)
    {
        return Integer.toHexString(Integer.parseInt(octalString, 8));
    }

    public static synchronized String decimalToBinary(String decimalString)
    {
        return Integer.toBinaryString(Integer.valueOf(decimalString));
    }

    public static synchronized String decimalToOctal(String decimalString)
    {
        return Integer.toOctalString(Integer.valueOf(decimalString));
    }

    public static synchronized String decimalToHexadecimal(String decimalString)
    {
        return Integer.toHexString(Integer.valueOf(decimalString));
    }

    public static synchronized String hexadecimalToBinary(String hexString)
    {
        return Integer.toBinaryString(Integer.parseInt(hexString, 16));
    }

    public static synchronized String hexadecimalToOctal(String hexString)
    {
        return Integer.toOctalString(Integer.parseInt(hexString, 16));
    }

    public static synchronized String hexadecimalToDecimal(String hexString)
    {
        return String.valueOf(Integer.parseInt(hexString, 16));
    }

    public static synchronized boolean isBinaryString(String binaryString)
    {
        for (int i = 0; i < binaryString.length(); i++)
        {
            if (!(binaryString.charAt(i) == '0' || binaryString.charAt(i) == '1'))
            {
                // String is not binary
                return false;
            }
        }
        return true;
    }

    public static synchronized boolean isOctalString(String octalString)
    {
        for (int i = 0; i < octalString.length(); i++)
        {
            int nextChar = octalString.charAt(i);
            if (!(nextChar >= '0' && nextChar <= '7'))
            {
                // String is not octal
                return false;
            }
        }
        return true;
    }

    public static synchronized boolean isDecimalString(String decimalString)
    {
        for (int i = 0; i < decimalString.length(); i++)
        {
            int nextChar = Integer.valueOf(String.valueOf(decimalString.charAt(i)));
            if (!(nextChar >= 0 && nextChar <= 9))
            {
                // String is not decimal
                return false;
            }
        }
        return true;
    }

    /**
     * Hexadecimal strings may contain either digits 0 to 9, characters a to f
     * or characters A to F.
     * <p>
     * @param hexString
     *                  <p>
     * @return
     */
    public static synchronized boolean isHexadecimalString(String hexString)
    {
        for (int i = 0; i < hexString.length(); i++)
        {
            int nextChar = hexString.charAt(i);
            if (!((nextChar >= '0' && nextChar <= '9') || (nextChar >= 'a' && nextChar <= 'f') || (nextChar >= 'A' && nextChar <= 'F')))
            {
                // String is not decimal
                return false;
            }
        }
        return true;
    }
}

<h1> Base conversion tools</h1>
<p>Static threadsafe methods for converting between binary, octal, decimal and hexadecimal.</p>